import os

MONGO = {
    'HOST': os.getenv('DB_HOST', 'localhost'),
    'PORT': os.getenv('DB_PORT', '27017'),
    'NAME': os.getenv('DB_NAME', 'ideen'),

}

KAFKA = {
    'BOOTSTRAP_SERVER': os.getenv('KAFKA_BOOTSTRAP_SERVER', 'localhost:9092').split(','),
    'INPUT_TOPIC': os.getenv('KAFKA_INPUT_TOPIC', 'input')
}