import os

from pymongo import MongoClient
import json
from kafka import KafkaConsumer, TopicPartition

from setting import MONGO, KAFKA

import logging

logging.basicConfig(
    level=int(os.environ.get("DEBUG_LEVEL", logging.INFO)),
    format="[%(asctime)s][%(process)d][%(thread)d]:" + logging.BASIC_FORMAT,
    handlers=[
        logging.StreamHandler()
    ]
)

log = logging.getLogger(__name__)


def main():
    log.info('starting...')
    log.info(f"connecting to postgres {MONGO['HOST']}:{MONGO['PORT']}")
    client = MongoClient("localhost", 27017)
    ideen_db = client.ideen
    patents_collection = ideen_db.patents_0_raw  # change for need topic
    log.info(f"connecting to kafka {KAFKA['BOOTSTRAP_SERVER']}")
    topic_name = "patents-0-raw"
    consumer_group = "ideen-1"
    consumer = KafkaConsumer(
        bootstrap_servers=['localhost:9092'],
        auto_offset_reset='latest',
        consumer_timeout_ms=10000,
        group_id=consumer_group,
        enable_auto_commit=False,
        auto_commit_interval_ms=1000
    )
    topic_partition = TopicPartition(topic_name, 0)
    assigned_topic = [topic_partition]
    consumer.assign(assigned_topic)
    consumer.seek_to_beginning(topic_partition)
    i = 0
    log.info(f"read data from topic {topic_name}")
    for message in consumer:
        try:
            parsed_message = json.loads(message.value.decode('utf-8'))
            patents_collection.insert_one(parsed_message)
            log.debug('The message number %d has been sent to mongo' % i)
            i += 1
        except Exception as e:
            log.error('--> It seems an Error occurred: {}'.format(e))
    consumer.commit()
    log.info("task finished")


if __name__ == "__main__":
    main()